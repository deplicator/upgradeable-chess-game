

Pieces can be upgraded and downgraded. Why downgrade?

Pieces have a point value associated with them, your entire team has a point value of all your
combine pieces. It may be advantageous to downgrade some pieces to bring down your total team score.
Pieces can have multiple downgrade and upgrade paths.

Some quick Pawn downgrade ideas:
- reduce movement by 1 (can only move on its first turn one square, then never move again)
- reduce movement by 2 (can never move)
- remove first move option
- make first move mandatory
- cannot capture enemies
- cannot be upgraded in row 8 (row 8 upgrade will need to be different from normal chess)
- can only start adjacent to Noble
- can only start adjacent to Royal
- can never move more than 3 squares from Noble (valid squares check for Noble, if Noble moves too far away the piece cannot move)
- can never move more than 3 squares from Royal

Say a Pawn is worth 10 points, the downgrades may reduce it's value by 1 or 2 points. Conflicting
downgrade should be made unavailable when the first one is chosen.


Piece Classification:
All pieces fall into one of three classifications.

- Footman (maybe a better name)
- Noble
- Royal

The only Footman in standard chess is the Pawn. The Footman is traditionally limited in what they
can do and have low point values.

Nobles are Knights, Rooks, Bishops and Queens in standard Chess. These traditionally have a lot of
versatility. Note the Queen, despite her royal sounding name, is a Noble.

Royals are the pieces required to be captured to win. This would be the King in standard chess, but
in our game this can be multiple pieces. Though, they are quite expensive point wise and have
limited function. Valid teams must contain at least one.


Standard Move: the move a piece makes anytime
First Move: the optional or mandatory move a piece makes on its first move
Optional Move: an optional move that can be taken at any time, may have a counter associated with it.

Row 8 pawn (or other piece) upgrade ideas:
- a piece will specifically have to have this ability to upgrade
- upgrade could be in the form of immediate bonus to that piece (like the player choses a free piece upgrade from the tier tree with no cost of experience points)
- or could be the return of a fallen piece, this gives the advantage of bringing back a piece that could gain experience at the end of the game.

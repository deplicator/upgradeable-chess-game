
bare minimum user stories
-------------------------
The bullets marked with `-` are implemented, bullets with `+` are areas that
need improvement.

- _Story:_ A player must be able to login.
    - can login, creates a token for session auth
    - token must be passed with each model fetch (set to all model fetches in app.js with jQuery's ajaxSend)
    - when logged in player can see data in protected routes

    + could be improved with session model
    + token saved unencrypted in local storage
    + partial player data saved unencrypted in local storage
    + no registration validation
    + no login validation
    + username shouldn't be case sensitive for login or registration validation (but should show case in views)


- _Story:_ A player must be able to navigate between views.
    - Anyone can navigate to /, /welcome, /registration, /login
    - with a token players can navigate to /dashboard and /profile
    - man subviews are complicated

    + !subviews has broken this
    + completely new view, page is refreshed on navigation change--could be better with nav view and subviews
    + bug: protected routes can be navigated to, but nothing shows (should redirect to welcome)
    + bug: situation where token is expired but in localStorage causes it to loop between /welcome and /dashboard


- _Story:_ A player must have an editable profile.
    - route for profile created
    - player can change username (will effect login), email (no validation),
      and a description field

    + cannot change password, need to special case for bcrypt-ed password when updating profile
    + interface to change profile fields could be less clunky
    + player model should have child object for profile data (only id will be in root)


- _Story:_ A player must be able to view their inventory.
    - When player registers they get 16 default chess pieces.
        - Player model has been updated to include an inventory array which holds the piece if of pieces they own.
        - Piece model and mongo collection has been created.
        - Piece model can be passed a class (pawn, bishop, etc..).
    - Client side piece model and backbone collection.
    - Collection of player owned pieces created when user logs in.
    - Piece collection shown in dashboard (for now).
    - Piece models use a subview.

    + pretyy up subview

- _Story:_ A player must be able to acquire new pieces.
    - temporarily have a make new piece button
        - creates a client side model pawn
        - post to server side to create model of pawn
        - updates player inventoryIndex
        - saves piece to database
        - client side, adds piece to player piece collection and renders new piece in interface
    - pieces can be deleted

    + obviously there needs to be limits on new piece creation, but this is fine for now.
    + confirmation on piece delete

- _Story:_ A player must be able to create and edit teams.
    + update player object for teams of pieces


- _Story:_ A player must be able to start a game against other players with a chosen team.


- _Story:_ A player must be able to edit pieces.





http://stackoverflow.com/questions/9916073/how-to-load-bootstrapped-models-in-backbone-js-while-using-amd-require-js/10288587#10288587


Views
-----
### unprotected
- welcome
- register
- login

### protected
- dashboard
    - inventory
        - piece
    - team overview
        - team details
- profile
- play (during game)


Collections
-----------
- player pieces


Models
------
- player
- piece

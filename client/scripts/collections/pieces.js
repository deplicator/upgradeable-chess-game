define([
        'underscore','backbone', 'globals',
        'models/piece'
    ],
    function(_, Backbone, Globals, PieceModel) {

        PiecesCollection = Backbone.Collection.extend({
            model: PieceModel,
            url: '/api/piece/',

            initialize: function() {

                // When a piece is added to collection, get it's latest attributes from server.
                // If the piece is not currently in player's inventoryIndex, add it.
                this.on('add', function(model) {
                    var modelId = model.get('_id');
                    var inventoryIndex = Globals.player.model.get('inventoryIndex');
                    if (inventoryIndex.indexOf(modelId) === -1) {
                        inventoryIndex.push(modelId);
                        Globals.player.model.set('inventoryIndex', inventoryIndex);
                        Globals.player.model.save();
                    }

                    model.fetch({success: function() {
                        //console.log('fethced', model.toJSON())
                    }});
                });

                // When a piece is removed from this collection, update player model inventoryIndex.
                this.on('remove', function(model) {
                    var modelId = model.get('_id');
                    var inventoryIndex = Globals.player.model.get('inventoryIndex');
                    inventoryIndex.splice(inventoryIndex.indexOf(modelId), 1);
                    Globals.player.model.set('inventoryIndex', inventoryIndex);
                    Globals.player.model.save();
                });
            }
        });

        return PiecesCollection;
    }
);

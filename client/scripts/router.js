
define([
        'jquery', 'underscore', 'backbone', 'globals',
        'views/dashboard', 'views/profile',
        'views/register','views/login', 'views/welcome', 'views/footer'
    ],

    function($, _, Backbone, Globals, DashboardView, ProfileView,
             RegisterView, LoginView, WelcomeView, FooterView) {

        Router = Backbone.Router.extend({
            routes: {
                'dashboard': 'showDashboard',
                'profile'  : 'showProfile',
                'register' : 'showRegistration',
                'login'    : 'showLogin',
                'logout'   : 'doLogout',
                'welcome'  : 'defaultAction',
                '*actions' : 'defaultAction'
            },

            execute: function(callback, args, name) {
                //console.log(callback, args, name)

                if (callback) callback.apply(this, args);
            },

            showDashboard: function() {

                var dashboardView = new DashboardView();
            },

            showProfile: function() {
                var profileView = new ProfileView();
                var footerView = new FooterView();
            },

            showRegistration: function() {
                var registerView = new RegisterView();
                registerView.render();
                var footerView = new FooterView();
            },

            showLogin: function() {
                var loginView = new LoginView();
                loginView.render();
                var footerView = new FooterView();
            },

            doLogout: function() {
                localStorage.removeItem('token');
                localStorage.removeItem('tokenExpiration');
                localStorage.removeItem('player');
                Globals.views.navigation.remove();
                this.navigate('welcome', true);
            },

            defaultAction: function() {
                var welcomeView = new WelcomeView();
                welcomeView.render();
                var footerView = new FooterView();
            }

        });

        return Router;

    }
);

// Filename: app.js
define([
    'jquery', 'underscore', 'backbone', 'globals',
    'router', 'models/player', 'collections/pieces', 'views/protected'

], function($, _, Backbone, Globals, Router, PlayerModel, PieceCollection, ProtectedView) {

    var initialize = function() {

        // Send access token with ajax calls.
        $(document).ajaxSend(function(event, request) {
            request.setRequestHeader('x-access-token', localStorage.getItem('token'));
        });

        // Create a rotuer.
        var AppRouter = new Router();

        // Start history.
        Backbone.history.start({pushState: true});

        // Check for returning player.
        // [TODO]: check for valid token
        // [TODO]: allow refresh on protected paths
        if (localStorage.getItem('player')) {

            Globals.player.model = new PlayerModel(JSON.parse(localStorage.getItem('player')));
            Globals.player.inventory = new PieceCollection();

            // If the player is valid, update the global player model.
            Globals.player.model.fetch({
                success: function() {

                    // Add player's piece inventory to new Piece Collection.
                    var inventoryIndex = Globals.player.model.get('inventoryIndex');
                    for (var i = 0; i < inventoryIndex.length; i++) {

                        // Piece models are fetched on add.
                        Globals.player.inventory.add({_id: inventoryIndex[i]});
                    }

                    // Call the main private view.
                    Globals.views.protected = new ProtectedView();

                    // Navigate to dashboard.
                    AppRouter.navigate('dashboard', true);
                }
            });
        }
    };

    return {
        initialize: initialize
    };
});

define([
        'underscore','backbone',
    ],
    function(_, Backbone) {

        PieceModel = Backbone.Model.extend({

            idAttribute: '_id',

            urlRoot: '/api/piece'

        });

        return PieceModel;
    }
);

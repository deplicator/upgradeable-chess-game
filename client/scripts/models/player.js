define([
        'underscore','backbone',
    ],
    function(_, Backbone) {

        var PlayerModel = Backbone.Model.extend({

            defautls: {
                inventoryIndex: []
            },

            initialize: function(options) {
                this.player = options;
            },

            url: function() {
                return 'api/player/' + this.player.id;
            },

            parse: function(response) {

                // Authentication problem (no token or invalid)
                if(response.success === false) {
                    window.location.href = '/welcome';
                }

                // Failed to get player data from server.
                if(response.name === 'CastError') {
                    window.location.href = '/welcome';
                }

                delete response._id;
                delete response.password;
                delete response.__v;
                return response;
            }
        });

        return PlayerModel;
    }
);

define([
        'jquery', 'underscore', 'backbone',
        'text!templates/profile.html',
        'models/player',
        'models/piece'
    ],

    function($, _, Backbone, ProfileTemplate, PlayerModel, PieceModel) {

        var HomeView = Backbone.View.extend({

            events: {
                'click .editable': 'edit',
                'click .editing-save': 'save'
            },

            el: $('#page'),

            template: _.template(ProfileTemplate),

            initialize: function() {
                var self = this;

                this.model = new PlayerModel(JSON.parse(localStorage.getItem('player')));
                this.model.fetch({
                    success: function() {
                        self.render();
                    }
                });
            },

            render: function() {
                //var navView = new NavView({model: this.model});
                //navView.render();

                var compiledTemplate = this.template({player: this.model.toJSON()});
                this.$el.html(compiledTemplate);
            },

            edit: function(e) {
                $(e.currentTarget).hide();
                $(e.currentTarget).siblings('.editing').removeClass('hidden');
            },

            save: function(e) {
                var field = $(e.currentTarget).siblings('input, textarea').attr('name');
                var value = $(e.currentTarget).siblings('input, textarea').val();
                this.model.set(field, value);
                this.model.save(null, {
                        success: function(response) {
                            $(e.currentTarget).parent().addClass('hidden');
                            $(e.currentTarget).parent().siblings('.editable').html(value);
                            $(e.currentTarget).parent().siblings('.editable').show();
                        }
                    }
                );
            }
        });

        return HomeView;

    }
);


//[TODO]: validation as you type

define([
        'jquery', 'underscore', 'backbone',
        'text!templates/register.html'
    ],

    function($, _, Backbone, registerTemplate) {

        var RegisterView = Backbone.View.extend({

            events: {
                'click #register-submit': 'register',
                'keyup #register-username': 'validation'
            },

            el: $('#page'),

            render: function() {

                $('.menu li').removeClass('active');
                $('.menu li a[href="#"]').parent().addClass('active');

                this.$el.html(registerTemplate);

            },

            register: function (e) {
                e.preventDefault();
                $.ajax({
                    method: 'POST',
                    url: '/api/register',
                    data: $('#register-form').serialize(),
                    dataType: 'json',
                    complete: function (data) {
                        var result = JSON.parse(data.responseText);
                        if (result.success) {
                            window.location.href = '/login';
                        } else {
                            $('#register-error').html(result.message);
                        }
                    },
                    error: function (data) {
                        console.log(data);
                        $('#register-error').html(data);
                    }
                });
            },

            validation: function () {
                if ($('#register-username').val().length > 3) {
                    $.ajax({
                        method: 'POST',
                        url: '/api/checkusername',
                        data: $('#register-form').serialize(),
                        dataType: 'json',
                        complete: function (data) {
                            var result = JSON.parse(data.responseText);
                            $('#register-error').html(result.message);
                        },
                        error: function (data) {
                            console.log(data);
                            $('#register-error').html(data);
                        }
                    });
                }
            }


        });

        return RegisterView;

    }
);

define([
        'jquery', 'underscore', 'backbone', 'globals',
        'views/navigation', 'views/dashboard', 'views/footer'
    ],

    function($, _, Backbone, Globals, NavigationView, DashboardView, FooterView) {

        var ProtectedView = Backbone.View.extend({

            initialize: function() {
                this.model = Globals.player.model;
                this.collection = Globals.player.inventory;

                Globals.views.navigation = new NavigationView({playerName: Globals.player.model.get('username')});
                Globals.views.dashboard = new DashboardView();
                Globals.views.footer = new FooterView();
            }
        });

        return ProtectedView;
    }
);

define([
        'jquery', 'underscore', 'backbone',
        'text!templates/login.html'
    ],

    function($, _, Backbone, loginTemplate) {

        var LoginView = Backbone.View.extend({

            events: {
                'click #login-submit': 'login'
            },

            el: $('#page'),

            render: function() {

                this.$el.html(loginTemplate);

            },

            /**
             * Submit login form to server.
             */
            // [TODO]: add validation
            // [TODO]: turn into model
            login: function(e) {
                e.preventDefault();
                $.ajax({
                    method: 'POST',
                    url: '/api/authenticate',
                    data: $('#login-form').serialize(),
                    dataType: 'json',
                    complete: function (data) {
                        var result = JSON.parse(data.responseText);
                        if (result.success) {
                            localStorage.setItem('token', result.token);
                            localStorage.setItem('tokenExpiration', result.tokenExpiration);
                            localStorage.setItem('player', JSON.stringify(result.player));
                            window.location.href = '/dashboard';
                        } else {
                            $('#login-error').html(result.message);
                        }
                    },
                    error: function (data) {
                        console.log(data);
                        $('#login-error').html(data);
                    }
                });
            }

        });

        return LoginView;

    }
);

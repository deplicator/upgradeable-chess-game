define([
        'jquery', 'underscore', 'backbone', 'bootstrap', 'globals', 'router',
        'text!templates/nav.html'
    ],

    function($, _, Backbone, Bootstrap, Globals, Router, NavTemplate) {

        var NavView = Backbone.View.extend({

            el: $('nav'),

            events: {
                'click #nav-logout': 'logout',
                'click #nav-profile': 'profile'
            },

            initialize: function(options) {
                this.playerName = options.playerName;
                this.router = new Router();
                this.render();
            },

            render: function () {
                var compiledTemplate = _.template(NavTemplate)({playerName: this.playerName});
                this.$el.html(compiledTemplate);
            },

            logout: function () {
                this.router.navigate('logout', true);
            },

            profile: function(e) {
                e.preventDefault();
                this.router.navigate('profile', true);
            }

        });

        return NavView;
    }
);

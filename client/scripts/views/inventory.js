/**
 *
 */

define([
        'jquery', 'underscore', 'backbone', 'globals',
        'views/piece', 'models/piece', 'text!templates/inventory.html',
    ],

    function($, _, Backbone, Globals, PieceView, PieceModel, InventoryTemplate) {
        'use strict';

        var InventoryView = Backbone.View.extend({

            template: _.template(InventoryTemplate),

            events: {
                'click #piece': 'makePiece'
            },

            initialize: function(options) {
                this.model = Globals.player.model;
                this.collection = Globals.player.inventory;

                if(this.collection) {
                    this.collection.on('change', this.render, this);
                    this.render();
                }
            },

            render: function() {
                this.$el.html(this.template());
                this.addPieces();
            },

            /**
             * Create a single piece view for all the pieces in the collection.
             */
            addPieces: function() {
                this.collection.each(this.addPiece, this);
            },

            /**
             * Append a single piece view to the piece list.
             */
            addPiece: function(model, index, collection) {

                // New view for this piece.
                var view = new PieceView({model: model});

                // Append the view.
                $(this.el).find('#piece-list').append(view.el);
                return this;
            },

            /**
             * Create a new piece, save it to database, add it to collection,
             * update player's inventoryIndex.
             */
            makePiece: function() {
                var self = this;

                // Create new piece.
                var newPiece = new PieceModel();

                // Add view for this piece.
                this.addPiece(newPiece);

                // Save this piece to database.
                newPiece.save(null, {
                    success: function(model, response) {

                        // Update collection, which will get all it's attributes
                        // from the server and trigger a render update. It will
                        // also update the player's inventoryIndex.
                        self.collection.add(model);
                    }
                });
            }
        });

        return InventoryView;
    }
);

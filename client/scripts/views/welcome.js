define([
        'jquery', 'underscore', 'backbone',
        'text!templates/welcome.html'
    ],

    function($, _, Backbone, welcomeTemplate) {

        var HomeView = Backbone.View.extend({

            el: $('#page'),

            initialize: function () {
                $(window).scroll(this.parallax);
            },

            render: function() {
                this.$el.html(welcomeTemplate);
            },

            /**
             * Welcome page parallax effect.
             */
            parallax: function () {
                var jumboHeight = $('.jumbotron').outerHeight();
                function parallax(){
                    scrolled = $(window).scrollTop();
                    $('.bg').css('height', (jumboHeight-scrolled) + 'px');
                }

                $(window).scroll(function(e){
                    parallax();
                });
            },

        });

        return HomeView;

    }
);

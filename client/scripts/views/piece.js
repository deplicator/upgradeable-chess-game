define([
        'jquery', 'underscore', 'backbone',
        'collections/pieces',
        'models/piece', 'text!templates/piece.html',
    ],

    function($, _, Backbone, PieceCollection, PieceModel, PieceTemplate) {

        var PieceView = Backbone.View.extend({
            tagName: 'li',
            className: 'piece',
            id: function() {
                return 'piece-' + this.model.get('_id');
            },
            template: _.template(PieceTemplate),

            events: {
                'click .piece-delete': 'deletePiece'
            },

            initialize: function(options) {
                this.model.on('change', this.render, this);
                this.render();
            },

            render: function() {
                this.$el.html(this.template({piece: this.model.toJSON()}));
                return this;
            },

            /**
             * Deletes a players piece from the database perminantly.
             */
            deletePiece: function() {
                // [TODO]: add confirmation.
                // Remove DOM element.
                this.remove();

                // Remove this model from client side piece collection.
                // PiecesCollection updates player's inventoryIndex.
                this.model.collection.remove(this.model);

                // Remove this model from server side.
                this.model.destroy();
            }
        });

        return PieceView;

    }
);

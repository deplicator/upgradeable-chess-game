define([
        'jquery', 'underscore', 'backbone', 'globals',
        'text!templates/dashboard.html',
        'views/inventory',
        'collections/pieces',
        'models/player',
        'models/piece'
    ],

    function($, _, Backbone, Globals, DashboardTemplate, InventoryView, PieceCollection, PlayerModel, PieceModel) {

        var DashboardView = Backbone.View.extend({

            events: {

            },

            el: $('#page'),

            template: _.template(DashboardTemplate),

            initialize: function() {
                this.model = Globals.player.model;
                this.collection = Globals.player.inventory;

                Globals.views.inventory = new InventoryView();

                $('#page').append(this.$el);
                if(this.model) {
                    this.model.on('change', this.render, this);
                    this.render();
                }
            },

            render: function() {
                var compiledTemplate = this.template({player: this.model.toJSON()});
                this.$el.html(compiledTemplate);
                this.$el.find('#inventory').append(Globals.views.inventory.$el);

                return this;
            }

        });

        return DashboardView;
    }
);

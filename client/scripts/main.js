require.config({
    paths: {
        jquery: 'libs/jquery.min',
        jscookie: 'libs/js-cookie',
        underscore: 'libs/underscore',
        backbone: 'libs/backbone',
        bootstrap: 'libs/bootstrap.min',
        text: 'libs/text',
        templates: '../templates',
        globals: 'globals'
    }
});

require(['app'], function(App) {
    App.initialize();
});

// -------------------------------------------------------------------- Packages
var express = require('express');
var bodyParser = require('body-parser');
var morgan = require('morgan');


// ---------------------------------------------------------------------- Models
var User = require('./models/player');


// --------------------------------------------------------------- Configuration
var config = require('./config');
var app = express();
var port = config.port;

// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// use morgan to log requests to the console
app.use(morgan('dev'));


// ------------------------------------------------------------------ API Routes
// Add API to app.
app.use('/api', require('./routers/api'));


// -------------------------------------------------------------------Web Routes
app.use('/fonts', express.static('public/fonts'));
app.use('/images', express.static('public/images'));
app.use('/scipts', express.static('public/scripts'));
app.use('/styles', express.static('public/styles'));
app.use('/', require('./routers/web'));


// ---------------------------------------------------------------- Start Server
var server = app.listen(port, function() {
    var host = server.address().address;
    var port = server.address().port;
    console.log('Server running at http://' + host + ':' + port);
});

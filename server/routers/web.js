// -------------------------------------------------------------------- Packages
var express = require('express');
var mongoose = require('mongoose');
var path = require('path');


// ---------------------------------------------------------------------- Models
var Player = require('../models/player');


// --------------------------------------------------------------- Configuration
var config = require('../config');
var app = express();
var routes = express.Router();

// Set embeded javascript as template engine.
app.set('view engine', 'html');

var webRoot = path.join(__dirname, '..','..', 'client');


// ---------------------------------------------------------------------- Routes
// Routes handled client side.
routes.get(['/', '/register', '/welcome', '/login'], function(req, res) {
    res.sendFile(path.join(webRoot, 'public.html'));
});

// Routes handled client side.
routes.get(['/dashboard', '/profile', '/game'], function(req, res) {
    res.sendFile(path.join(webRoot, 'index.html'));
});

// Sends any other file as is.
routes.get('*', function(req, res) {
    //This is the current file they have requested
    var file = req.params[0];

    //Send the requesting client the file.
    res.sendFile(file, {root: webRoot});
});

module.exports = routes;

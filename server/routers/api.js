// -------------------------------------------------------------------- Packages
var express = require('express');
var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt-nodejs');
var mongoose = require('mongoose');


// ---------------------------------------------------------------------- Models
var Player = require('../models/player');
var Piece = require('../models/piece');


// --------------------------------------------------------------- Configuration
var config = require('../config');
var routes = express.Router();

// Database connection.
mongoose.connect(config.database.connectionString);

// ---------------------------------------------------------- Unprotected Routes
// Create a new player.
routes.post('/register', function(req, res) {
    //[TODO]: username and password validation
    //[TODO]: email verification

    // Search for player by username.
    Player.findOne({username: req.body.username}, function(err, player) {
        if (err) {
            throw err;
        }

        // If this user name is taken.
        if (player) {
            res.json({
                success: false,
                message: 'Registration failed: player already exists.'
            });

        // Otherwise create a new player.
        } else {
            var newPlayer = new Player();

            // Add players registration information.
            newPlayer.username = req.body.username;
            newPlayer.password = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10), null);
            newPlayer.email = req.body.email;

            // Set up one team for the player.
            newPlayer.teams[0] = {
                teamId: 1,
                name: 'My first team',
                value: 100,
                pieces: []
            }

            // Give new player a default inventory.
            newPlayer.inventoryIndex = [];
            var newPiece;

            // Give a player 3x as many pawns to make a default team.
            for (var i = 0; i < 24; i++) {
                newPiece = new Piece();

                // add first 8 to team
                if (i < 8) {
                    newPiece.teamId = 1;
                    newPlayer.teams[0].pieces.push(newPiece._id);
                }

                newPiece.save();
                newPlayer.inventoryIndex.push(newPiece._id);
            }

            // Give a player 3x as many rooks to make a default team.
            for (i = 0; i < 6; i++) {
                newPiece = new Piece({class: 'rook', value: 20});
                newPiece.save();
                newPlayer.inventoryIndex.push(newPiece._id);

                // add first 2 to team
                if (i < 2) {
                    newPlayer.teams[0].pieces.push(newPiece._id);
                }
            }

            // Give a player 3x as many knights to make a default team.
            for (i = 0; i < 6; i++) {
                newPiece = new Piece({class: 'knight', value: 20});
                newPiece.save();
                newPlayer.inventoryIndex.push(newPiece._id);

                // add first 2 to team
                if (i < 2) {
                    newPlayer.teams[0].pieces.push(newPiece._id);
                }
            }

            // Give a player 3x as many bishops to make a default team.
            for (i = 0; i < 6; i++) {
                newPiece = new Piece({class: 'bishop', value: 20});
                newPiece.save();
                newPlayer.inventoryIndex.push(newPiece._id);

                // add first 2 to team
                if (i < 2) {
                    newPlayer.teams[0].pieces.push(newPiece._id);
                }
            }

            // Give a player 3x as many queens to make a default team.
            for (i = 0; i < 3; i++) {
                newPiece = new Piece({class: 'queen', value: 30});
                newPiece.save();
                newPlayer.inventoryIndex.push(newPiece._id);

                // add first to team
                if (i < 1) {
                    newPlayer.teams[0].pieces.push(newPiece._id);
                }
            }

            // Give a player 3x as many kings to make a default team.
            for (i = 0; i < 3; i++) {
                newPiece = new Piece({class: 'royal', value: 15});
                newPiece.save();
                newPlayer.inventoryIndex.push(newPiece._id);

                // add first to team
                if (i < 1) {
                    newPlayer.teams[0].pieces.push(newPiece._id);
                }
            }

            // Save new player to database.
            newPlayer.save(function(err) {
                if (err) {
                    throw err;
                }

                res.json({
                    success: true,
                    message: 'Registration success: new player created.',
                });
            });
        }
    });
});

// Check for an existing username.
routes.post('/checkusername', function(req, res) {

    // Search for player by username.
    Player.findOne({username: req.body.username}, function(err, player) {
        if (err) throw err;

        // already exists
        if (player) {
            res.json({
                success: false,
                message: 'Player name already exists.'
            });
        } else {
            res.json({
                success: false,
                message: ''
            });
        }
    });
});

// Authenticate a player and give them a token.
routes.post('/authenticate', function(req, res) {

    // find the player
    Player.findOne({username: req.body.username}, function(err, player) {
        if (err) throw err;

        if (!player) {
            res.json({
                success: false,
                message: 'Authentication failed: player not found.'
            });

        } else if (player) {

            // check if password matches
            if (!bcrypt.compareSync(req.body.password, player.password)) {
                res.json({
                    success: false,
                    message: 'Authentication failed: incorrect password.'
                });

            } else {

                // Create a token.
                var token = jwt.sign(player, config.apiSecret, {
                    expiresIn: config.jwtExpiration
                });

                res.json({
                    success: true,
                    token: token,
                    tokenExpiration: Date.now() + 604800000,
                    player: {
                        id: player._id,
                        username: player.username
                    },
                    message: 'Authentication success: token created.'
                });
            }
        }

    });
});


// ------------------------------------------------------------ Protected Routes
// Check API routes that require authentication.
routes.use(function(req, res, next) {

    // check header or url parameters or post parameters for token
    var token = req.body.token || req.query.token ||
                req.params.token || req.headers['x-access-token'];

    // decode token
    if (token) {

        // verifies secret and checks exp
        jwt.verify(token, config.apiSecret, function(err, decoded) {
            if (err) {
                return res.json({
                    success: false,
                    message: 'Failed to authenticate token.'
                });
            } else {
                // if everything is good, save to request for use in other routes
                req.decoded = decoded;
                next();
            }
        });

    } else {

        // if there is no token
        return res.status(403).send({
            success: false,
            message: 'No token provided.'
        });
    }
});

// Is thi necessary? just for checking thigns
routes.post('/tokencheck', function(req, res) {
    res.json({
        success: true,
        message: 'Token valid',
    });
});

// Get player data.
routes.get('/player/:pid', function(req, res) {
    Player.findById(req.params.pid, function(err, player) {
        if (err) {
            res.json(err);
        } else {
            res.json(player);
        }
    });
});

// Save player data.
routes.put('/player/:pid', function(req, res) {
    Player.findOneAndUpdate(
        {'_id': req.params.pid},
        req.body,
        {upsert:true},
        function(err, doc){
            if (err) {
                res.json(err);
            } else {
                res.json('success');
            }
        }
    );
});

// create random piece for player
routes.post('/piece', function(req, res) {
    var piece;
    switch (Math.floor(Math.random() * 6)) {
        case 0:
            piece = {class: 'pawn', value: 10};
            break;

        case 1:
            piece = {class: 'rook', value: 20};
            break;

        case 2:
            piece = {class: 'knight', value: 20};
            break;

        case 3:
            piece = {class: 'bishop', value: 20};
            break;

        case 4:
            piece = {class: 'queen', value: 30};
            break;

        case 5:
            piece = {class: 'royal', value: 15};
            break;
    }
    var newPiece = new Piece(piece);
    newPiece.save();

    res.json(newPiece);
});

// Get piece data.
routes.get('/piece/:id', function(req, res) {
    Piece.findById(req.params.id, function(err, piece) {
        if (err) {
            res.json(err);
        } else {
            res.json(piece);
        }
    });
});


// Get piece data.
routes.delete('/piece/:id', function(req, res) {
    Piece.findById(req.params.id, function(err, piece) {
        if (err) {
            res.json(err);
        } else {
            piece.remove();
            res.json(req.params.id + ' deleted from database.');
        }
    });
});




module.exports = routes;

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


// Piece model schema.
var PieceSchema = new Schema({
    creationDate: {type: Date, default: Date.now},
    class: {type: String, default: 'pawn'},
    experience: {type: Number, default: 0},
    value: {type: Number, default: 10},
    teamId:  {type: Number, default: 0}
});

module.exports = mongoose.model('Piece', PieceSchema);

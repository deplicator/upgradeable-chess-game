var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Player model schema.
var PlayerSchema = new Schema({
    username: String,
    password: String,
    email: String,
    description: String,
    inventoryIndex: Array,
    inventory: {},
    teams: Array
});

module.exports = mongoose.model('Player', PlayerSchema);

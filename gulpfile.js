'use strict';

var gulp = require('gulp');
var nodemon = require('gulp-nodemon');      // Node monitor.
var clean = require('del');                 // For deleting files and directories.
var jshint = require('gulp-jshint');        // Analize JavaScript files.
var config = require('./server/config.json');

// Delete third party files in client directory.
gulp.task('clean', function () {
    return clean([
        'client/scripts/libs/**', '!client/scripts/libs/',
        'client/styles/libs/**', '!client/styles/libs/',
        'client/fonts/**', '!client/fonts/'
    ]);
});


// Copy js library files from bower_compontes to client.
gulp.task('copy:libs', function () {
    return gulp.src([
            'bower_components/requirejs/require.js',
            'bower_components/text/text.js',
            'bower_components/backbone/backbone.js',
            'bower_components/underscore/underscore.js',
            'bower_components/jquery/dist/*.*',
            '!bower_components/jquery/dist/*.slim.*',
            'bower_components/js-cookie/src/js.cookie.js',
            'bower_components/bootstrap/dist/js/*.*'
        ]).pipe(gulp.dest('client/scripts/libs/'));
});

// Copy css from bower_compontes to client.
gulp.task('copy:css', function () {
    return gulp.src([
            'bower_components/bootstrap/dist/css/*.*',
            'bower_components/font-awesome/css/*.*'
        ]).pipe(gulp.dest('client/styles/libs'));
});

// Copy fonts from bower_compontes to client.
gulp.task('copy:fonts', function () {
    return gulp.src([
            'bower_components/bootstrap/dist/fonts/*.*',
            'bower_components/font-awesome/fonts/*.*'
        ]).pipe(gulp.dest('client/fonts/'));
});

// Clean out and copy third party files back to client.
gulp.task('copy', ['clean', 'copy:libs', 'copy:css', 'copy:fonts']);

// Check js files for atrocities.
gulp.task('jshint', function() {
    return gulp.src([
            'server/**/*.js',
            'client/scripts/**/*.js', '!client/scripts/libs/**'
        ])
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// What to do after nodemon restart.
gulp.task('restart', ['jshint']);

// What happens when `gulp` is run from command line.
gulp.task('default', ['jshint', 'copy'], function () {
    var started = false;
    return nodemon({
        script: 'server/index.js',
        ext: 'html js css',
        ignore: ['.git/*', 'bower_compontes/*', 'node_modules/*', 'public/*', 'gulpfile.js']
    }).on('restart', function() {
        console.log('restarted');
        gulp.start('restart');
    }).on('start', function () {
        if (!started) {
            started = true;
        }
    });
});
